from django.urls import path
from receipts.views import (
    receipts_list,
    create_receipt,
    show_ExpenseCategory,
    show_Account,
    create_Category,
    create_Account,
)


urlpatterns = [
    path("", receipts_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", show_ExpenseCategory, name="category_list"),
    path("categories/create/", create_Category, name="create_category"),
    path("accounts/", show_Account, name="account_list"),
    path("accounts/create/", create_Account, name="create_account"),
]
